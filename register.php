<!DOCTYPE html>
<?php
  require('accounts.php');
  //require_once "recaptchalib.php";
  //add something for testing
  // your secret key
  $secret = "6LcH9JAUAAAAANNOnGeB46-UauekH79Y0mEQzTKQ";
 
  // empty response
  $response = null;
 
  // check secret key
  $userIP = $_SERVER['REMOTE_ADDR'];
  $userRes = $_POST['g-recaptcha-response'];

  // if submitted check response
  if ($_POST['g-recaptcha-response']) {
     $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$userRes&remoteip=$userIP";
     $response = file_get_contents($url);
     //print_r($response);
     $response = json_decode($response);
     //echo $response->success;
  }
    // If the values are posted, insert them into the database.
    if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2']) && isset($_POST['email'])){
        $username = $_POST['username'];
        $password = $_POST['password'];
        $password2 = $_POST['password2'];
        $email = $_POST['email'];

        if ($password != $password2){
          $fmsg = "Passwords do not Match!";
		    }elseif (preg_match('/\s/',$username) ){
			     $fmsg = "Username Contains Spaces!, please do not user spaces";
        }elseif ($response->success != 1){
           $fmsg = "Invalid Captcha";
        }else{
 		       $result = register($username, $password, $email);
          if($result){
              $smsg = "User Created Successfully. Please check your email for the activation link";
              #session_start();
              #$_SESSION['username'] = $username;
			       #header("location: members.php");
          }else{
              $fmsg ="User Registration Failed";
              #header("location: register.php");
          }
        }
    }
    ?>
<html style="min-height:100%">
<head>
	<title>Clone Wars Account Registration</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>

	<link rel="stylesheet" type="text/css" href="styles.css">

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#usernameLoading').hide();
			$('#username').keyup(function(){
			  $('#usernameLoading').show();
		      $.post("check.php", {
		        username: $('#username').val()
		      }, function(response){
		        $('#usernameResult').fadeOut();
		        setTimeout("finishAjax('usernameResult', '"+escape(response)+"')", 400);
		      });
		    	return false;
			});
		});

		function finishAjax(id, response) {
		  $('#usernameLoading').hide();
		  $('#'+id).html(unescape(response));
		  $('#'+id).fadeIn();
		} //finishAjax
	</script>

</head>
<body style="background-color:black;">
<div class="outer" style="position:relative;top:0px;margin:auto;width:1500px;height: 818px;background-image:url('AccountCreation.jpg');background-repeat: no-repeat;">
 <div class="container" style="padding-top:60px;display:block;margin:auto;">
	  <img src="TCWlogowhite.png" style="display:block;margin-left:auto;margin-right:auto;">
      <form class="form-signin" method="POST">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
          <div class="input-group">
		  <span class="input-group-addon" id="basic-addon1">@</span>
		  <input type="text" name="username" id="username" class="form-control" placeholder="Username" autocomplete="off" required autofocus>
		</div>
		<span id="usernameLoading"><img src="loading.gif" alt="Ajax Indicator" /></span>
		<span id="usernameResult"></span>
		
		<label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" autocomplete="off" required>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" autocomplete="off" required>
        <label for="inputPassword" class="sr-only">Password Again</label>
        <input type="password" name="password2" id="inputPassword2" class="form-control" placeholder="Password Again" autocomplete="off" required>
        <div class="g-recaptcha" data-sitekey="6LcH9JAUAAAAAEzdt_8YOzWCwBakgP_S1oup330H"></div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        <a class="btn btn-lg btn-primary btn-block" href="login.php">Login</a>
      </form>
<script src='https://www.google.com/recaptcha/api.js'></script>
</div>
</div>
</body>
</html>
