<?php  //Start the Session
session_start();
require('accounts.php');

//3.1.4 if the user is logged in Greets the user with message
if (isset($_SESSION['username'])){
  if (isset($_POST['cpassword']) && isset($_POST['password']) && isset($_POST['password2'])){
    $cpassword = $_POST['cpassword'];
    $password = $_POST['password'];
    $password2 = $_POST['password2'];
    $username = $_SESSION['username'];
    if ($password == $password2){
      changepass($username, $cpassword, $password);
    } else {
      $fmsg = "Passwords do not match!";
    }
  }
//3.2 When the user visits the page first time, simple login form will be displayed.
?>
<html>
<head>
	<title>Change Password -    <?php echo $_SESSION['username'];?></title>
	<h1>Change Password -    <?php echo $_SESSION['username'];?></h1>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

<link rel="stylesheet" href="styles.css" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
      <form class="form-signin" method="POST">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
      <h2 class="form-signin-heading">Change Password</h2>
    
      <label for="inputCurrentPassword" class="sr-only">Current Password</label>
      <input type="password" name="cpassword" id="inputCurrentPassword" class="form-control" placeholder="Current Password" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <label for="inputPassword2" class="sr-only">Password Again</label>
      <input type="password" name="password2" id="inputPassword2" class="form-control" placeholder="Password Again" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Change Password</button>
      <a class="btn btn-lg btn-primary btn-block" href="members.php">Control Panel</a>
      <a class="btn btn-lg btn-primary btn-block" href="logout.php">Logout</a>
      </form>
</div>

</body>

</html>
<?php
}else{
  echo "Unauthorized";
  }?>
