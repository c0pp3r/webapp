<?php  //Start the Session
session_start();
require('accounts.php');

//3.1.4 if the user is logged in Greets the user with message
if (isset($_SESSION['username'])){
//3.2 When the user visits the page first time, simple login form will be displayed.
?>
<html>
<head>
	<title>Control Panel -   <?php echo $_SESSION['username'];?> </title>
	<h1 style="color:white;padding-left: 30px; font-weight:bold;">Control Panel -   <?php echo $_SESSION['username'];?> </h1>
	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

<link rel="stylesheet" href="styles.css" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color:black;">
<div class="outer" style="position:relative;top:0px;margin:auto;width:1500px;height: 818px;background-image:url('AccountCreation.jpg');background-repeat: no-repeat;">
<div class="container">
<img src="TCWlogowhite.png" style="display:block;margin-left:auto;margin-right:auto;padding-bottom:20px;">
        <a class="btn btn-lg btn-primary btn-block" href="changePass.php">Change Password</a>
	<a class="btn btn-lg btn-primary btn-block" href="changeEmail.php">Change Email</a>	
	<?php if (isAdmin($_SESSION['username'])){echo '<a class="btn btn-lg btn-primary btn-block" href="accountAudit.php">Account Audit</a>';} ?>
	<?php if (isAdmin($_SESSION['username'])){echo '<a class="btn btn-lg btn-primary btn-block" href="accountSearch.php">Account Search</a>';} ?>
        <a class="btn btn-lg btn-primary btn-block" href="logout.php">Logout</a>
      </form>
</div>

</body>

</html>
<?php
}else{
  echo "Unauthorized";
  }?>
