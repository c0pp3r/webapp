<?php  //Start the Session
session_start();
require('accounts.php');

//3.1.4 if the user is logged in Greets the user with message
if (isset($_SESSION['username'])){
  if (isset($_POST['searchuser'])){
    $searchuser = $_POST['searchuser'];
    $username = $_SESSION['username'];
    header('Location: accountInfo.php?name='.$searchuser);
  }
//3.2 When the user visits the page first time, simple login form will be displayed.
?>
<html>
<head>
	<title>Account Search -    <?php echo $_SESSION['username'];?></title>
	<h1>Account Search -    <?php echo $_SESSION['username'];?></h1>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

<link rel="stylesheet" href="styles.css" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
      <form class="form-signin" method="POST">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
      <h2 class="form-signin-heading">Account Search</h2>
    
      <label for="searchuser" class="sr-only">Account Username</label>
      <input type="text" name="searchuser" id="searchuser" class="form-control" placeholder="Account Username" required autofocus>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Search</button>
      <a class="btn btn-lg btn-primary btn-block" href="members.php">Control Panel</a>
      <a class="btn btn-lg btn-primary btn-block" href="logout.php">Logout</a>
      </form>
</div>

</body>

</html>
<?php
}else{
  echo "Unauthorized";
  }?>
