<?php  //Start the Session
session_start();
 require('accounts.php');
//3. If the form is submitted or not.
//3.1 If the form is submitted
if (isset($_POST['username']) and isset($_POST['password'])){
//3.1.1 Assigning posted values to variables.
$username = $_POST['username'];
$password = $_POST['password'];
//3.1.2 Checking the values are existing in the database or not
$result = login($username, $password);
//3.1.2 If the posted values are equal to the database values, then session will be created for the user.
if ($result){
$_SESSION['username'] = $username;
header("location: members.php");
}else{
//3.1.3 If the login credentials doesn't match, he will be shown with an error message.
if (!isset($fmsg)){
$fmsg = "Invalid Login Credentials.";
}
}
}
//3.1.4 if the user is logged in Greets the user with message
//3.2 When the user visits the page first time, simple login form will be displayed.
?>
<html>
<head>
	<title>Clone Wars User Controlpanel Login</title>
	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

<link rel="stylesheet" href="styles.css" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background: url('AccountCreation.jpg') no-repeat center center fixed;
     position: absolute;
    /* these lines are the important bits  */
    height: 0;
    padding-bottom: 56.25%;
    box-sizing: border-box;

    width: 100%;
    margin-top: 100px;
    top: 0;
    /* bottom: 0; */
    left: 0;
    /* right: 0; */
    z-index: 0;
    background-position: 50% 50%; 
    -webkit-background-size: cover; 
    -moz-background-size: cover; 
    -o-background-size: cover; 
    background-size: cover;
    -moz-background-size: 100% 100%;
-webkit-background-size: 100% 100%;
background-size: 100% 100%;">

<form class="form-signin" method="POST">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
        <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">@</span>
	  <input type="text" name="username" class="form-control" placeholder="Username" autocomplete="off" required>
	</div>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" autocomplete="off" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
        <a class="btn btn-lg btn-primary btn-block" href="register.php">Register</a>
        <a class="btn btn-lg btn-primary btn-block" href="resetpass.php">Reset Password</a>
      </form>
</div>

</body>

</html>
<?php ?>