<?php  //Start the Session
session_start();
require('accounts.php');

//3.1.4 if the user is logged in Greets the user with message
if (isset($_SESSION['username']) && isAdmin($_SESSION['username'])){
//3.2 When the user visits the page first time, simple login form will be displayed.
?>
<html>
<head>
	<title>Account Audit</title>
	<h1>Account Audit</h1>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

<link rel="stylesheet" href="styles.css" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
      <a class="btn btn-lg btn-primary btn-block" onclick="window.history.back()">Back</a>
      <table style="width:100%">
        <tr>
          <th>IP</th>
          <th>Account Names</th>
          <th>All Accounts Whitelisted/Inactive?</th>
        </tr>
        <?php
        $ip = auditIpList();
        $list_result = false;
        $num = 1;
        //$test = accountsPerIp('24.74.4.199');
        //var_dump($test);

        foreach ($ip as $row){
            echo '<tr>';
            echo '<td>' . $row['ip'] . '</td>';
            //$acc_ids = accountsPerIp($row['ip']);
            echo '<td>';
            $count = 0; 
            $whitelisted = 0;
            for ($i = 0; $i < $row['account_count']; $i++){
              $acc_name = $row['username'];
              echo '<a href="accountInfo.php?name=' . $acc_name .'">' . $acc_name .'</a>,';
              if ($row['whitelisted'] == 1){
                $whitelisted += 1;
              } else if ($row['active'] == 0) {
                $whitelisted += 1;
              }
            }
            echo '</td>';
            if ($whitelisted == $row['account_count']){
              echo '<td><font color="green">Yes</font></td>';
            } else {
              echo '<td><font color="red">No</font></td>';
            }
            echo '</td>';
            echo '</tr>';


        }

        ?>
      </table>
      <a class="btn btn-lg btn-primary btn-block" onclick="window.history.back()">Back</a>
      
<?php
}else{
  echo "Unauthorized";
  }?>
