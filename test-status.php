<?php

// Written by Tyclo, adapted from status.php script (origin unknown)

require("simplexml.class.php");

$serverAddress = "149.56.100.90"; // Your server IP here
$port = 44455;

$fp = @fsockopen("tcp://" . $serverAddress, $port, $errno, $errstr, 5);

if (!$fp) {
    $status = "offline";
} else {
    $status = "online";
}

header('Content-type:application/json;charset=utf-8');
echo json_encode(array("status" => $status));
?>
