<?php
//DB Information
$host = '127.0.0.1';
$db   = 'swgemu';
$user = 'swgemu';
$pass = '123456';
$charset = 'utf8';
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);
//SWGEmu information
$dbsecret = 'swgemus3cr37!';


function login($username, $password){
	global $pdo, $dbsecret, $fmsg;
	$stmt = $pdo->prepare('SELECT salt FROM accounts where username = ?');
	$stmt->execute(array($username));
	$salt = $stmt->fetchColumn();
	$str = $dbsecret . $password . $salt;
	$hash = hash('sha256', $str);
	$stmt = $pdo->prepare('SELECT COUNT(*) FROM accounts where username = ? and password = ?');
	$stmt->execute(array($username, $hash));
	$result = $stmt->fetchColumn();
	if ($result == 0){
		return false;
	} else {
		$stmt = $pdo->prepare('SELECT active FROM accounts where username = ?');
		$stmt->execute(array($username));
		$result2 = $stmt->fetchColumn(0);
		if ($result2 == 1){
			return true;
		} else {
			$fmsg = 'Account is not active. Please check your email for an activation link.<a class="btn btn-lg btn-primary btn-block" href="resendactive.php">Resend Activation</a>';
			return false;
		}
	}
}

function register($username, $password, $email){
	global $pdo, $dbsecret;
	$stmt = $pdo->prepare('SELECT COUNT(*) FROM accounts where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn();
	if ($result > 0){
		return false;
	} else {
		$station = rand();
		$salt_full = hash('sha256', $station);
		$salt = substr($salt_full, 0, 32);
		$str = $dbsecret . $password . $salt;
		$hash = hash('sha256', $str);
		$reg = bin2hex(openssl_random_pseudo_bytes(16));
		$stmt = $pdo->prepare('INSERT INTO accounts (username, password, station_id, salt, active)
    	VALUES(?, ?, ?, ?, ?)');
    	$stmt2 = $pdo->prepare('INSERT INTO register (username, email, reghash)
    	VALUES(?,?,?)');
		$stmt->execute(array($username, $hash, $station, $salt, 0));
		$stmt2->execute(array($username, $email, $reg));
		sendActivation($username, $email, $reg);

		
		return true;
	}
}
function sendActivation($username, $email){
	global $pdo;
	$stmt = $pdo->prepare('SELECT reghash from register where username = ? and email = ?');
	$stmt->execute(array($username, $email));
	$reg = $stmt->fetchColumn(0);
	if ($reg){
		$to = $email;
		$subject = 'Clone Wars SWG Server Account Activation';
		$message = '
		Thanks for registering for Clone Wars SWG!
		Please follow the link below to activate your account:
		http://live.swgtcw.com/verify.php?username='.$username.'&hash='.$reg.'';
		$headers = 'From:noreply@swgtcw.com' . "\r\n";
		$delivery = 'smtp';
		mail($to, $subject, $message, $headers);
		return true;
	} else {
		return false;
	}

}
function sendReset($username, $email){
	global $pdo;
	$stmt = $pdo->prepare('SELECT reghash from register where username = ? and email = ?');
	$stmt->execute(array($username, $email));
	$reg = $stmt->fetchColumn(0);
	if ($reg){
		$to = $email;
		$subject = 'Clone Wars SWG Server Password Reset';
		$message = '
		A request has been made to reset your password. Please follow the link below to complete the reset process:
		http://live.swgtcw.com/reset.php?username='.$username.'&hash='.$reg.'';
		$headers = 'FROM:noreply@swgtcw.com' . "\r\n";
		$delivery = 'smtp';
		mail($to, $subject, $message, $headers);
		return true;
	} else {
		return false;
	}
}

function resetPass($username, $reghash, $newpass){
	global $pdo, $dbsecret, $fmsg, $smsg;
	$stmt = $pdo->prepare('SELECT reghash from register where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn(0);
	if ($reghash == $result){
		$stmt = $pdo->prepare('SELECT salt from accounts where username = ?');
		$stmt->execute(array($username));
		$salt = $stmt->fetchColumn(0);
		$str = $dbsecret . $newpass . $salt;
		$hash = hash(('sha256'), $str);
		$stmt = $pdo->prepare('UPDATE accounts set password = ?  where username = ?');
		$stmt->execute(array($hash, $username));
		$reg = bin2hex(openssl_random_pseudo_bytes(16));
                $stmt = $pdo->prepare('UPDATE register set reghash = ? where username = ?');
                $stmt->execute(array($reg, $username));
		$smsg = 'Password Change Successful! Return to Login Page:<a class="btn btn-lg btn-primary btn-block" href="login.php">Login</a>';    
	} else {
		$fmsg = 'Reset failed. Please try again or contact an administrator:<a class="btn btn-lg btn-primary btn-block" href="resetpass.php">Reset Password</a>';
	}
}

function checkRegHash($username, $reghash){
	global $pdo, $fmsg, $smsg;
	$stmt = $pdo->prepare('SELECT reghash from register where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn(0);
	if ($reghash == $result){
		return true;
	} else {
		return false;
	}
}

function checkName($username){
	global $pdo;
	$stmt = $pdo->prepare('SELECT COUNT(*) FROM accounts where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn();
	if ($result > 0){
		return true;
	} else {
		return false;
	}

}

function changepass($username, $password, $newpass){
	global $pdo, $dbsecret, $fmsg, $smsg;
	$stmt = $pdo->prepare('SELECT salt from accounts where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn(0);   
	$salt = $result;
	$str = $dbsecret . $password . $salt;
	$hash = hash('sha256', $str);
	$stmt = $pdo->prepare('SELECT COUNT(*) FROM accounts where username = ? and password = ?');
	$stmt->execute(array($username, $hash)); 
	$result = $stmt->fetchColumn();
	if ($result == 0){
		$fmsg = "Incorrect current password";
	}else {
		$str = $dbsecret . $newpass . $salt;
		$hash = hash(('sha256'), $str);
		$stmt = $pdo->prepare('UPDATE accounts set password = ?  where username = ?');
		$stmt->execute(array($hash, $username));
		$smsg = "Password Change Successful!";
	}
}

function changeemail($username, $newemail){
	global $pdo, $dbsecret, $fmsg, $smsg;
	$stmt = $pdo->prepare('UPDATE register set email = ? where username = ?');
	$result = $stmt->execute(array($newemail, $username));
	if ($result == false){
		$fmsg = "Could not change email";
	}else{
		$smsg = "Email changed successfully";
	}
}

function currentemail($username){
	global $pdo, $dbsecret;
	$stmt = $pdo->prepare('SELECT email from register where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn(0);
	return $result;
}

function isAdmin($username){
	global $pdo;
	$stmt = $pdo->prepare('SELECT admin_level from accounts where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn(0);
	if ($result < 10){
		return false;
	}else {
		return true;
	}
}

function verify($username, $reghash){
	global $pdo;
	$stmt = $pdo->prepare('SELECT reghash from register where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn(0);
	if ($reghash == $result){
		$reg = bin2hex(openssl_random_pseudo_bytes(16));
		$stmt = $pdo->prepare('UPDATE accounts set active = 1 where username = ?');
		$stmt->execute(array($username));
		$stmt = $pdo->prepare('UPDATE register set reghash = ? where username = ?');
		$stmt->execute(array($reg, $username));
		return true;
	} else {
		return false;
	}
}

function adminChangePass($username, $newpass){
	global $pdo, $dbsecret;
	$stmt = $pdo->prepare('SELECT salt from accounts where username = ?');
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn(0);   
	$salt = $result;
	$str = $dbsecret . $newpass . $salt;
	$hash = hash(('sha256'), $str);
	$stmt = $pdo->prepare('UPDATE accounts set password = ?  where username = ?');
	$stmt->execute(array($hash, $username));
	echo "\n" . $username . " Password set to " . $newpass . "\n";
}

function listIps(){
	global $pdo;
	$sql = "select distinct ip from account_ips where timestamp > '2018-02-01 00:00:00'";
	$stmt = $pdo->query($sql);
	$ip = array();
	$ip = $stmt->fetchAll();
	return $ip;
}

function auditIpList(){
	global $pdo;
	$sql = "SELECT DISTINCT account_ips.ip,
		dupe_ips.account_count,
		accounts.username,
		register.whitelisted,
		accounts.active
		FROM account_ips
		INNER JOIN (
  			SELECT account_ips.ip,
  			COUNT(DISTINCT account_ips.account_id) account_count
  			FROM account_ips
  			GROUP BY account_ips.ip
  			HAVING COUNT(DISTINCT account_ips.account_id) > 1
		) dupe_ips
		ON dupe_ips.ip = account_ips.ip
		INNER JOIN accounts
		ON accounts.account_id = account_ips.account_id
		INNER JOIN register
		ON register.username = accounts.username
		ORDER BY account_ips.ip,
		accounts.username";
	$stmt = $pdo->query($sql);
	$ip = array();
	$ip = $stmt->fetchAll();
	return $ip;
}

function accountsPerIpCount($ip){
	global $pdo;
	$stmt = $pdo->prepare('select count(distinct account_id) from account_ips where ip = ?');
	$stmt->execute(array($ip));
	$result =  $stmt->fetchColumn();
	return $result;
}

function accountsPerIp($ip){
	global $pdo;
	$stmt = $pdo->prepare('select distinct account_id from account_ips where ip = ?');
	$stmt->execute(array($ip));
	$result = $stmt->fetchAll();
	return $result;
}

function accountNamefromId($id){
	global $pdo;
	$stmt = $pdo->prepare('select username from accounts where account_id = ?');
	$stmt->execute(array($id));
	return $stmt->fetchColumn();
}

function accountPullInfo($id){
	global $pdo;
	$stmt = $pdo->prepare('select accounts.account_id, accounts.username, accounts.active, r1.whitelisted from accounts LEFT JOIN register r1 on accounts.username = r1.username where accounts.account_id = ?');
	$stmt->execute(array($id));
	$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
	return $result;
}

function accountWhitelisted($name){
	global $pdo;
	$stmt = $pdo->prepare('select whitelisted from register where username = ?');
	$stmt->execute(array($name));
	$result = $stmt->fetchColumn(0);
	if ($result == 0){
		return false;
	} else {
		return true;
	}
}

function accountActive($id){
	global $pdo;
	$stmt = $pdo->prepare('select active from accounts where account_id = ?');
	$stmt->execute(array($id));
	return $stmt->fetchColumn();
}

function accountInformation($name){
	global $pdo;
	$stmt = $pdo->prepare('select account_id, created, active, admin_level from accounts where username = ?');
	$stmt->execute(array($name));
	$result = $stmt->fetchAll();
	return $result;
}

function accountCharacters($id){
	global $pdo;
	$stmt = $pdo->prepare('select firstname, surname, creation_date from characters where account_id = ?');
	$stmt->execute(array($id));
	$result = $stmt->fetchAll();
	return $result;
}

function ipsPerAccount($id){
	global $pdo;
	$stmt = $pdo->prepare('select distinct ip from account_ips where account_id = ?');
	$stmt->execute(array($id));
	$result = $stmt->fetchAll();
	return $result;
}

function toggleActive($name, $reason, $admin){
	global $pdo;
	$stmt = $pdo->prepare('select active from accounts where username = ?');
	$stmt->execute(array($name));
	$status = $stmt->fetchColumn(0);
	if ($status > 0){
		$active = 0;
	} else {
		$active = 1;
	}
	$stmt = $pdo->prepare('UPDATE accounts set active = ? where username = ?');
	$stmt2 = $pdo->prepare('UPDATE register set inactive_reason = ?, inactive_admin = ? where username = ?');
    $stmt->execute(array($active, $name));
    $stmt2->execute(array($reason, $admin, $name));
}

function toggleWhitelisted($name, $reason, $admin){
	global $pdo;
	$whitelisted = accountWhitelisted($name);
	if ($whitelisted){
		$change = 0;
	} else {
		$change = 1;
	}
	$stmt = $pdo->prepare('UPDATE register set whitelisted = ?,whitelist_admin = ?, whitelist_reason = ? where username = ?');
	$stmt->execute(array($change, $admin, $reason, $name));
}

function lastLogin($id){
	global $pdo;
	$stmt = $pdo->prepare('SELECT idaccount_ips, timestamp from account_ips where account_id = ? ORDER BY idaccount_ips DESC LIMIT 1');
	$stmt->execute(array($id));
	$last = $stmt->fetchColumn(1);

	return $last;
}

function whitelistInfo($name){
	global $pdo;
	$stmt = $pdo->prepare('SELECT whitelist_admin, whitelist_reason, inactive_admin, inactive_reason from register where username = ?');
	$stmt->execute(array($name));
	$result = $stmt->fetchAll();
	return $result;
}



//$str = 'swgemus3cr37!' . 'password' . '4933d97e16c2666f7e315a407571d15c';
//login('c0pp3r1', 'cawcaw');
//register('c0pp3r5', 'blahblah', 'test@test.com');
//changepass('c0pp3r1', 'cawcaw', 'blahblah');
//isAdmin('c0pp3r');
//adminChangePass('c0pp3r1', 'cawcaw');
//sendActivation('test', 'dominguez.joey@gmail.com');
?>
