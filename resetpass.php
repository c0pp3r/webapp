<?php
require('accounts.php');
if (isset($_GET['username']) and isset($_GET['email'])){
//3.1.1 Assigning posted values to variables.
  $username = $_GET['username'];
  $email = $_GET['email'];
  $success = sendReset($username, $email);
  if ($success){
    $smsg = "Password Reset Email Sent";
  } else {
    $fmsg = "Failure, please check your credentials and/or contact an administrator";
  }

//3.1.4 if the user is logged in Greets the user with message

//3.2 When the user visits the page first time, simple login form will be displayed.
}
?>
<html>
<head>
	<title>Reset Password</title>
	<h1 style="color:white;padding-left: 30px; font-weight:bold;">Reset Password</h1>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

<link rel="stylesheet" href="styles.css" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background: url('AccountCreation.jpg') no-repeat center center fixed;
     position: absolute;
    /* these lines are the important bits */
    height: 0;
    padding-bottom: 56.25%;
    box-sizing: border-box;

    width: 100%;
    margin-top: 100px;
    top: 0;
    /* bottom: 0; */
    left: 0;
    /* right: 0; */
    z-index: 0;
    background-position: 50% 50%; 
    -webkit-background-size: cover; 
    -moz-background-size: cover; 
    -o-background-size: cover; 
    background-size: cover;
    -moz-background-size: 100% 100%;
-webkit-background-size: 100% 100%;
background-size: 100% 100%;">

<form class="form-signin" method="GET">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
        <h2 class="form-signin-heading">Reset Password</h2>
        <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">@</span>
    <input type="text" name="username" class="form-control" placeholder="Username" autocomplete="off" required>
  </div>
        <label for="inputEmail" class="sr-only">email</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" autocomplete="off" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
        <a class="btn btn-lg btn-primary btn-block" href="login.php">Login</a>
      </form>
</div>

</body>

</html>
