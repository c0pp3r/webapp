<?php  //Start the Session
require('accounts.php');

//3.1.4 if the user is logged in Greets the user with message
if(isset($_GET) & !empty($_GET)){
  $username = $_GET['username'];
  $reghash = $_GET['hash'];
  $auth = checkRegHash($username, $reghash);
  if (!$auth){
    echo "Oops, something went wrong. Try reset again or contact an administrator";
    header("location: login.php");
    exit();
  }
  if (isset($_POST['password']) && isset($_POST['password2'])){
    $password = $_POST['password'];
    $password2 = $_POST['password2'];
    if ($password == $password2){
      resetPass($username, $reghash, $password);
    } else {
      $fmsg = "Passwords do not match!";
    }
  }
//3.2 When the user visits the page first time, simple login form will be displayed.
?>
<html>
<head>
	<title>Reset Password -    <?php echo $_GET['username'];?></title>
	<h1 style="color:white;padding-left: 30px; font-weight:bold;">Reset Password -    <?php echo $_GET['username'];?></h1>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

<link rel="stylesheet" href="styles.css" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background: url('AccountCreation.jpg') no-repeat center center fixed;
     position: absolute;
    /* these lines are the important bits  */
    height: 0;
    padding-bottom: 56.25%;
    box-sizing: border-box;

    width: 100%;
    margin-top: 100px;
    top: 0;
    /* bottom: 0; */
    left: 0;
    /* right: 0; */
    z-index: 0;
    background-position: 50% 50%; 
    -webkit-background-size: cover; 
    -moz-background-size: cover; 
    -o-background-size: cover; 
    background-size: cover;
    -moz-background-size: 100% 100%;
-webkit-background-size: 100% 100%;
background-size: 100% 100%;">

<div class="container">
      <form class="form-signin" method="POST">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>    
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" autocomplete="off" required autofocus>
      <label for="inputPassword2" class="sr-only">Password Again</label>
      <input type="password" name="password2" id="inputPassword2" class="form-control" placeholder="Password Again" autocomplete="off" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Change Password</button>
      </form>
</div>

</body>

</html>
<?php
}else{
  echo "Unauthorized";
  }?>
