<?php  //Start the Session
session_start();
require('accounts.php');

//3.1.4 if the user is logged in Greets the user with message
if (isset($_SESSION['username']) && isAdmin($_SESSION['username']) && isset($_GET['name']) && isset($_GET['cmd']) && isset($_GET['reason']) && !empty($_GET['cmd']) && !empty($_GET['name'])){
	$user = $_GET['name'];
	$cmd = $_GET['cmd'];
	$admin = $_SESSION['username'];
	$reason = $_GET['reason'];
	if ($cmd == 'active'){
		toggleActive($user, $reason, $admin);
		header('Location: ' . 'accountInfo.php?name=' . $user);
	} elseif ($cmd == 'whitelist') {
		toggleWhitelisted($user, $reason, $admin);
		header('Location: ' . 'accountInfo.php?name=' . $user);
	}
	
}

if (isset($_SESSION['username']) && isAdmin($_SESSION['username']) && isset($_GET['name']) && !empty($_GET['name'])){
	$namer = $_GET['name'];
	$info = accountInformation($_GET['name']);
	if ($info == NULL){
		echo '<h1> USER DOES NOT EXIST </h1>';
		die();
	}
	$toons  = accountCharacters($info[0]['account_id']);
	$ips = ipsPerAccount($info[0]['account_id']);
	$created = $info[0]['created'];
	$lastlogin = lastLogin($info[0]['account_id']);
	$whitelistedNum = accountWhitelisted($_GET['name']);
	$activeNum = accountActive($info[0]['account_id']);
	$whitelistInfo = whitelistInfo($_GET['name']);
	if ($whitelistedNum == 0){
		$whitelisted = "False";
	}else {
		$whitelisted = "True";
	}
	if ($activeNum == 0){
		$active = "False";
	}else {
		$active = "True";
	}

?>
<html>
<head>
	<title>Account Information -   <?php echo $_GET['name'];?> </title>
	<h1>Account Information -   <?php echo $_GET['name'];?> </h1>
	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

<link rel="stylesheet" href="styles.css" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<p>&nbsp;</p>
<h2 style="color: #2e6c80;">&nbsp;</h2>
<table class="editorDemoTable" style="height: 276px;" width="805" border="3">
<tbody>
<tr>
<td style="width: 457px;">Account Name</td>
<td style="width: 330px;"><?php echo $_GET['name'];?></td>
</tr>
<tr>
<td style="width: 457px;">Active</td>
<td style="width: 330px;"><?php echo $active;?>
<tr>
<tr>
<td style="width: 457px;">Active status changed by</td>
<td style="width: 330px;"><?php echo $whitelistInfo[0]['inactive_admin'];?>
<tr>
<tr>
<td style="width: 457px;">Active status reason</td>
<td style="width: 330px;"><?php echo $whitelistInfo[0]['inactive_reason'];?>
<tr>
<td style="width: 457px;">Character Names</td>
<td style="width: 330px;"><span style="color: #008000;"><span style="font-size: 13px;"><?php foreach ($toons as $toon){echo $toon['firstname'] . " " . $toon['surname'] . ", ";}?> </span></span></td>
</tr>
<tr>
<td style="width: 457px;">IP Addresses</td>
<td style="width: 330px;"><span style="color: #FF0000;"><span style="font-size: 13px;"><?php foreach ($ips as $ip){echo $ip['ip'] . ", ";}?></td>
</tr>
<tr>
<td style="width: 457px;">Creation Date</td>
<td style="width: 330px;"><?php echo $created;?></td>
</tr>
<tr>
<td style="width: 457px;">Last Login</td>
<td style="width: 330px;"><?php echo $lastlogin;?></td>
</tr>
<tr>
<td style="width: 457px;">Multiple Account Whitelisting</td>
<td style="width: 330px;"><?php echo $whitelisted;?>
</tr>
<tr>
<td style="width: 457px;">Whitelisting Admin</td>
<td style="width: 330px;"><?php echo $whitelistInfo[0]['whitelist_admin'];?>
</tr>
<tr>
<td style="width: 457px;">Whitelisting Reasoning</td>
<td style="width: 330px;"><?php echo $whitelistInfo[0]['whitelist_reason'];?>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<div class="container">
    <!--<?php echo '<a class="btn btn-lg btn-primary btn-block" href="accountInfo.php?name=' . $namer . '&cmd=whitelist' . '"' . '>Toggle Whitelisted</a>' ?> -->
	<!--<?php echo '<a class="btn btn-lg btn-primary btn-block" href="accountInfo.php?name=' . $namer . '&cmd=active' . '"' . '>Toggle Active</a>' ?>	-->
	<a class="btn btn-lg btn-primary btn-block" onclick="whitelist()">Toggle Whitelisted</a>
	<a class="btn btn-lg btn-primary btn-block" onclick="active()">Toggle Active</a>
    <a class="btn btn-lg btn-primary btn-block" onclick="window.history.back()">Back</a>
      </form>
</div>
<script>
function whitelist() {
    var reason = prompt("Reason for whitelisting", "no reason given");
    var name = '<?php echo $namer;?>';
    if (reason == null || reason == "") {
        alert("No reason given");
        window.location.href = "accountInfo.php?name=" + name;
    } else {
        window.location.href = "accountInfo.php?name=" + name + "&cmd=whitelist&reason=" + reason;
    }
}
function active() {
    var reason2 = prompt("Reason for setting inactive", "no reason given");
    var name = '<?php echo $namer;?>';
    if (reason2 == null || reason2 == "") {
        window.location.href = "accountInfo.php?name=" + name;
    } else {
		window.location.href = "accountInfo.php?name=" + name + "&cmd=active&reason=" + reason2;
    }
}
</script>

</body>

</html>
<?php
}else{
  echo "Unauthorized Acces or empty command/name";
  }?>
